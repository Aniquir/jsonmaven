import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by RENT on 2017-08-21.
 */
public class Person {

    String name;
    String surName;
    List<Integer> list = new ArrayList<Integer>(Arrays.asList(new Integer(1), new Integer(15)));
    int[] array = {1,4};
    String[] sArray = {"Pierwszy String", "Drugi String"};
    boolean aBoolean = true;
    boolean bBoolean = false;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }
}
