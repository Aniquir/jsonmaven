import com.google.gson.Gson;

import javax.print.DocFlavor;
import java.io.*;

/**
 * Created by RENT on 2017-08-21.
 */
public class Main {

    public static void main(String[] args){

//        Person person = new Person();
//
//        person.setName("Lukas");
//        person.setSurName("Sajnog");
//
//        Gson gson = new Gson();
//        String out = gson.toJson(person);
//
//        System.out.println(out);
//-----

        //tworzymy inputStream
//            InputStream inputStream = null;
//            try {
//                inputStream = new FileInputStream("temp.txt");
//                inputStream.read();
//
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }catch (IOException e){
//                e.printStackTrace();
//            }finally {
//                try {
//                    inputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
        //-------
        //tworzymy plik txt, mozemy jsonowy, generujemy plik z danymi jsonowymi
        //jezeli mamy do dyspozycji java8 to korzystamy z tej wersji tego(nie ma do tego dostepu w androidzie)
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("temp.txt"))){
//
//            writer.write(gson.toJson(person));
//
//        } catch (IOException e){
//
//        }
        //odczytujemy z pliku, zpaisujemy do Stringa i ze Stringa  tworzymy nowy obiekt
        try (BufferedReader reader = new BufferedReader(new FileReader("temp1.txt"))){

            String read = reader.readLine();
            System.out.println(read);

            Gson gson2 = new Gson();
            Person personFromReader = gson2.fromJson(read, Person.class);
        } catch (IOException e){

        }

    }
}
